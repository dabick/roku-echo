def buildResponse(text = None, content = None):
    return {
        'version':'1.0',
        'sessionAttributes': {},
        'response': buildSpeechResponse(text, content)
    }

def buildSpeechResponse(text = None, content = None):
    response = {
        'shouldEndSession': True
    }
    if text is not None:
        response['outputSpeech'] = {
            'type': 'PlainText',
            'text': text
        }
    if content is not None:
        response['card'] = {
            'type': 'Simple',
            'title': 'Roku-Echo',
            'content': content
        }
    return response
