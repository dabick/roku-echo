import requests
import boto3
import os

from base64 import b64decode

import response

encryptedUsername = os.environ["username"]
encryptedPassword = os.environ["password"]
encryptedHost = os.environ["host"]
encryptedPort = os.environ["port"]
encryptedAppId = os.environ["appId"]

# Decrypt code should run once and variables stored outside of the function
# handler so that these are decrypted once per container
username = boto3.client('kms').decrypt(CiphertextBlob=b64decode(encryptedUsername))['Plaintext']
password = boto3.client('kms').decrypt(CiphertextBlob=b64decode(encryptedPassword))['Plaintext']
host = boto3.client('kms').decrypt(CiphertextBlob=b64decode(encryptedHost))['Plaintext']
port = boto3.client('kms').decrypt(CiphertextBlob=b64decode(encryptedPort))['Plaintext']
appId = boto3.client('kms').decrypt(CiphertextBlob=b64decode(encryptedAppId))['Plaintext']

def handleLaunch(request):
    print "Currently launching does nothing"
    return response.buildResponse("Roku-Echo has nothing to launch", None)

def home(slots):
    command("/roku/home")
    return response.buildResponse(text="Going Home")

def rewind(slots):
    command("/roku/rewind")
    return response.buildResponse()

def fastForward(slots):
    command("/roku/fastForward")
    return response.buildResponse()

def play(slots):
    command("/roku/play")
    return response.buildResponse()

def select(slots):
    command("/roku/select")
    return response.buildResponse()

def left(slots):
    command("/roku/left")
    return response.buildResponse()

def right(slots):
    command("/roku/right")
    return response.buildResponse()

def down(slots):
    command("/roku/down")
    return response.buildResponse()

def up(slots):
    command("/roku/up")
    return response.buildResponse()

def back(slots):
    command("/roku/back")
    return response.buildResponse()

def replay(slots):
    command("/roku/replay")
    return response.buildResponse()

def info(slots):
    command("/roku/info")
    return response.buildResponse()

def backspace(slots):
    command("/roku/backspace")
    return response.buildResponse()

def search(slots):
    command("/roku/search")
    return response.buildResponse()

def enter(slots):
    command("/roku/enter")
    return response.buildResponse()

def letter(slots):
    command("/roku/letter")
    return response.buildResponse()

def letters(slots):
    command("/roku/letters")
    return response.buildResponse()

def netflix(slots):
    command("/roku/netflix")
    return response.buildResponse()

def command(path, data=None):
    url = host + ":" + port + path
    r = requests.post(url, auth=(username, password), data=data)
    print "Sent command to Roku and it returned ", r.status_code

intentHandlers = {
    "Home": home,
    "Rewind": rewind,
    "FastForward": fastForward,
    "PlayPause": play,
    "Select": select,
    "Left": left,
    "Right": right,
    "Down": down,
    "Up": up,
    "Back": back,
    "Replay": replay,
    "Info": info,
    "Backspace": backspace,
    "Search": search,
    "Enter": enter,
    "Letter": letter,
    "Letters": letters,
    "Netflix": netflix
}

def handleIntent(request):
    intent = request["intent"]
    return intentHandlers[intent["name"]](intent.get("slots", None))

def handleSessionEnd(request):
    print "Ending Session"

requestHandlers = {"LaunchRequest": handleLaunch, "IntentRequest": handleIntent, "SessionEndedRequest": handleSessionEnd}

def handler(event, context):
    print "event.session.applicationId is %s" % event["session"]["application"]["applicationId"]

    if (event["session"]["application"]["applicationId"] != appId):
        raise ValueError("Invalid Application ID")

    request = event["request"]
    alexaResponse = requestHandlers[request["type"]](request)
    print alexaResponse
    return alexaResponse