package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	FILENAME  = "options.json"
	SEARCH    = "M-SEARCH * HTTP/1.1\r\nHost: 239.255.255.250:1900\r\nMan: \"ssdp:discover\"\r\nST: roku:ecp\r\n\r\n"
	USERNAME  = "username"
	PASSWORD  = "password"
	PORT      = "port"
	PATH      = "path"
	CERT_FILE = "certFile"
	KEY_FILE  = "keyFile"
)

type Roku struct {
	cacheLength        int
	name               string
	location           string
	lastNotified       time.Time
	uniqueSerialNumber string
	//state ??
}

var (
	conn     net.PacketConn
	rokus    = make([]Roku, 0) //todo add mutex on this and make it a map name -> USN. By default name = USN
	username = ""
	password = ""
	port     = ""
	path     = ""
	certFile = ""
	keyFile  = ""
	options  = make(map[string]interface{}, 0)
)

func init() {
	if err := loadOptions(); err != nil {
		log.Fatal("Failed to load options " + err.Error())
	}
	username = options[USERNAME].(string)
	password = options[PASSWORD].(string)
	port = options[PORT].(string)
	path = options[PATH].(string)
	certFile = options[CERT_FILE].(string)
	keyFile = options[KEY_FILE].(string)
}

//todo Should use my config library after it's been refactored to be a map
func loadOptions() error {
	file, err := ioutil.ReadFile(FILENAME)
	if err != nil {
		return err
	}

	if err = json.Unmarshal(file, &options); err != nil {
		return err
	}

	log.Println("Loaded options")

	return nil
}

func main() {
	setup()
	defer conn.Close()

	go listen()

	runSearch()

	http.Handle("/roku/home", interceptor{home})
	http.Handle("/roku/rewind", interceptor{rewind})
	http.Handle("/roku/fastForward", interceptor{fastForward})
	http.Handle("/roku/play", interceptor{play})
	http.Handle("/roku/select", interceptor{makeSelect})
	http.Handle("/roku/left", interceptor{left})
	http.Handle("/roku/right", interceptor{right})
	http.Handle("/roku/down", interceptor{down})
	http.Handle("/roku/up", interceptor{up})
	http.Handle("/roku/back", interceptor{back})
	http.Handle("/roku/replay", interceptor{instantReplay})
	http.Handle("/roku/info", interceptor{info})
	http.Handle("/roku/backspace", interceptor{backspace})
	http.Handle("/roku/search", interceptor{search})
	http.Handle("/roku/enter", interceptor{enter})
	http.Handle("/roku/letter", interceptor{letter})
	http.Handle("/roku/letters", interceptor{letters})
	http.Handle("/roku/netflix", interceptor{netflix})
	err := http.ListenAndServeTLS(":"+port, path+certFile, path+keyFile, nil)
	if err != nil {
		log.Fatal(err)
	}
}

type interceptor struct {
	handleFunc func(http.ResponseWriter, *http.Request)
}

func (c interceptor) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	user, pass, ok := r.BasicAuth()
	if ok && user == username && pass == password {
		c.handleFunc(w, r)
		return
	}
	log.Println("Wanted " + username + " --- " + password)
	log.Println("Got " + user + "---" + pass)
	w.WriteHeader(401)
}

func home(w http.ResponseWriter, r *http.Request) {
	keyPress("Home", w)
}

func rewind(w http.ResponseWriter, r *http.Request) {
	keyPress("Rev", w)
}

func fastForward(w http.ResponseWriter, r *http.Request) {
	keyPress("Fwd", w)
}

func play(w http.ResponseWriter, r *http.Request) {
	keyPress("Play", w)
}

func makeSelect(w http.ResponseWriter, r *http.Request) {
	keyPress("Select", w)
}

func left(w http.ResponseWriter, r *http.Request) {
	keyPress("Left", w)
}

func right(w http.ResponseWriter, r *http.Request) {
	keyPress("Right", w)
}

func down(w http.ResponseWriter, r *http.Request) {
	keyPress("Down", w)
}

func up(w http.ResponseWriter, r *http.Request) {
	keyPress("Up", w)
}

func back(w http.ResponseWriter, r *http.Request) {
	keyPress("Back", w)
}

func instantReplay(w http.ResponseWriter, r *http.Request) {
	keyPress("InstantReplay", w)
}

func info(w http.ResponseWriter, r *http.Request) {
	keyPress("Info", w)
}

func backspace(w http.ResponseWriter, r *http.Request) {
	keyPress("Backspace", w)
}

func search(w http.ResponseWriter, r *http.Request) {
	keyPress("Search", w)
}

func enter(w http.ResponseWriter, r *http.Request) {
	keyPress("Enter", w)
}

func letter(w http.ResponseWriter, r *http.Request) {
	keyPress("Lit_a", w)
}

func letters(w http.ResponseWriter, r *http.Request) {
	keyPress("Lit_b", w)
}

func keyPress(key string, w http.ResponseWriter) {
	if err := sendCommand("keypress/" + key); err != nil {
		handleError(w, err)
	}
}

func netflix(w http.ResponseWriter, r *http.Request) {
	if err := launch("12"); err != nil {
		handleError(w, err)
	}
}

func handleError(w http.ResponseWriter, err error) {
	log.Println(err)
	w.WriteHeader(500)
}

func launch(appId string) error {
	return sendCommand("launch/" + appId)
}

func sendCommand(url string) error {
	found := false
	for _, roku := range rokus {
		found = true
		sending := roku.location + url
		log.Println(sending)
		req, err := http.NewRequest("POST", sending, nil)
		if err != nil {
			return err
		}
		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 && resp.StatusCode != 204 {
			log.Println(resp)
			return errors.New("Roku returned non-200/204 status.")
		}
	}
	if !found {
		return errors.New("Failed to find any rokus")
	}
	return nil
}

func setup() {
	var err error
	conn, err = net.ListenPacket("udp4", ":8081")
	if err != nil {
		log.Fatal(err)
	}
}

func runSearch() {
	go rokuSearch()
}

func rokuSearch() {
	remoteAddress, err := net.ResolveUDPAddr("udp4", "239.255.255.250:1900")
	if err != nil {
		log.Println(err)
	}

	_, err = conn.WriteTo([]byte(SEARCH), remoteAddress)
	if err != nil {
		log.Println(err)
	}

	log.Println("Sent out a call for all Rokus!.")
}

func listen() {
	buf := make([]byte, 1024)
	for {
		rlen, addr, err := conn.ReadFrom(buf)
		if err != nil {
			log.Println(err)
		} else if addr == nil {
			continue
		}
		handleRead(buf, rlen)
	}
}

func handleRead(buf []byte, rlen int) {
	if len(buf) == 0 {
		return
	}

	response := string(buf)
	log.Println(response)

	lines := strings.Split(response, "\r\n")
	httpLine := strings.SplitN(lines[0], " ", 3)
	if strings.ToUpper(httpLine[0][:4]) == "HTTP" {
		handleResponse(lines, httpLine)
	} else {
		handleRequest(lines, httpLine)
	}
}

func handleRequest(lines, httpLine []string) {
	if strings.ToUpper(httpLine[0][:6]) == "NOTIFY" {
		log.Println("Received Notify")
		log.Println(lines)
	}
}

func handleResponse(lines, httpLine []string) {
	if strings.ToUpper(httpLine[0][:4]) != "HTTP" {
		log.Println("Wasn't HTTP.")
		return
	} else if strings.TrimSpace(httpLine[1]) != "200" {
		log.Print("Received ")
		log.Print(httpLine[1])
		log.Print(" status " + httpLine[2] + " instead of 200.\n")
		return
	}

	headers, bodyStart := handleHeader(lines)
	if len(headers) == 3 {
		log.Println("Started with " + strconv.Itoa(len(rokus)) + " rokus")
		found := false
		for _, roku := range rokus {
			if roku.uniqueSerialNumber == headers["USN"] {
				roku.lastNotified = time.Now()
				found = true
				break
			}
		}
		if !found {
			cache, err := strconv.Atoi(headers["CACHE-CONTROL"])
			if err != nil {
				log.Println("Invalid cache " + headers["CACHE-CONTROL"])
				return
			}

			newRoku := Roku{}
			newRoku.cacheLength = cache
			newRoku.lastNotified = time.Now()
			newRoku.location = headers["LOCATION"]
			newRoku.uniqueSerialNumber = headers["USN"]

			log.Println("Adding roku")
			rokus = append(rokus, newRoku)
		}
		log.Println("Ended with " + strconv.Itoa(len(rokus)) + " rokus")
	} else {
		log.Println("Got invalid response")
		log.Println(headers)
	}

	_ = handleBody(lines, bodyStart)
}

func handleHeader(lines []string) (map[string]string, int) {
	headers := make(map[string]string, 3)
	bodyStart := -1
	for i := 1; i < len(lines); i++ {
		if len(lines[i]) == 0 {
			bodyStart = i + 1
			break
		}
		parts := strings.SplitN(lines[i], ":", 2)
		field := strings.ToUpper(strings.TrimSpace(parts[0]))
		value := strings.TrimSpace(parts[1])
		switch field {
		case "LOCATION":
			value = value
			if value[len(value)-1] != '/' {
				value += "/"
			}
		case "USN":
			value = value[14:]
		case "CACHE-CONTROL":
			value = value[8:]
		default:
			value = ""
		}
		if len(value) > 0 {
			log.Println(value)
			headers[field] = value
		}
	}
	return headers, bodyStart
}

func handleBody(lines []string, i int) string {
	if i == len(lines) {
		return ""
	}
	body := ""
	for ; i < len(lines); i++ {
		if len(lines[i]) == 0 {
			continue
		}
		body += lines[i]
	}
	return body
}
