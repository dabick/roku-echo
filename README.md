This project allows you to interact with your Roku for your TV via your Amazon Echo/Echo Dot.
It uses Golang for the backend server running on your home network to talk to your Roku and a python script running in an AWS Lambda to send commands to your home Roku server.

To execute python code, need to get the requests library and install it in the lambda folder.
<code>pip install requests</code>

Then upload the 2 JSON files in /skills into the Alexa dashboard
Then zip everything in lambda into a zip folder and upload it via the AWS Lambda dashboard.
Provide the environment variables (with encryption) necessary in the AWS Lambda dashboard.

Build/Install roku.go.
Provide an options.json file with the necessary information in the same directotry and the roku executable.

Run the roku server. Ensure your router's port forwarding is setup to pass through to your roku server.
Talk to Alexa and watch your Roku come alive!